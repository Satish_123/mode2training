package com.eLibrary.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.eLibrary.dao.UserDao;
import com.eLibrary.exception.UserNotFoundException;
import com.eLibrary.model.User;
import com.eLibrary.service.UserService;

@RestController
@Validated
public class UserController {

	@Autowired
	private UserService userService;
	@Autowired
    private UserDao repository;
	@PostMapping(value = "/add")
	public ResponseEntity<User> addActor(@Valid @RequestBody User user) {
		User addUser = userService.addUser(user);
		return new ResponseEntity<User>(addUser, HttpStatus.OK);
	}

	@GetMapping(value = "/all")
	public ResponseEntity<List<User>> getUsers() {
		List<User> getUsers = userService.getAllUsers();
		return new ResponseEntity<List<User>>(getUsers, HttpStatus.OK);
	}

	@GetMapping(value = "/getuser/{id}")
	public User getUserById(@PathVariable("id") @NotNull Integer id) {

		return this.repository.findById(id).orElseThrow(()->new UserNotFoundException("User Not Found with this id::"+id));

	}

	@DeleteMapping(value = "/deleteuser/{id}")
	public ResponseEntity<User> deleteUserById(@PathVariable("id") Integer id) {
		User existingUser=this.repository.findById(id).orElseThrow(()->new UserNotFoundException("User Not Found with this id::"+id));
		this.repository.delete(existingUser);
		
		return new ResponseEntity<User>(existingUser, HttpStatus.OK);

	}

	@PutMapping(value = "/update")
	public ResponseEntity<User> updateUser(@Valid @RequestBody User user) {
		User updateUser = userService.addUser(user);
		return new ResponseEntity<User>(updateUser, HttpStatus.ACCEPTED);
	}

}
