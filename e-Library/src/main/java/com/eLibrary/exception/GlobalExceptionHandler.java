package com.eLibrary.exception;



import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

	//Handle specific exception
	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<?> resourceNotFoundException(UserNotFoundException ex,WebRequest request){
		
		ErrorDetails details=new ErrorDetails(new Date(),ex.getMessage(),request.getDescription(false));
		return new ResponseEntity(details,HttpStatus.NOT_FOUND);
	}
	
	//Handle global exception
	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> globalExceptionHandling(Exception ex,WebRequest request){
		
		ErrorDetails detail=new ErrorDetails(new Date(),ex.getMessage(),request.getDescription(false));
		return new ResponseEntity(detail,HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
