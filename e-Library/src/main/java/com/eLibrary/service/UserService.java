package com.eLibrary.service;

import java.util.List;

import com.eLibrary.model.User;

public interface UserService {

	User addUser(User user);

	List<User> getAllUsers();

	User getUserById(Integer id);

	Integer deleteUserById(Integer id);
}
