package com.eLibrary.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.eLibrary.dao.UserDao;
import com.eLibrary.model.User;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao userRepository;
	JdbcTemplate is;

	@Override
	public User addUser(User user) {

		return userRepository.save(user);
	}

	@Override
	public List<User> getAllUsers() {

		return userRepository.findAll();
	}

	@Override
	public User getUserById(Integer id) {

		/*
		 * return userRepository.getById(id); //Lazy loading so to avoid we added
		 * extension in application.properties(or changes in pojo) and we can get only
		 * instance to it but not access to DB
		 */
		return userRepository.findById(id).get(); // optional classes //EagerLoading

	}

	@Override
	public Integer deleteUserById(Integer id) {
		userRepository.deleteById(id);
		return id;
	}

}
