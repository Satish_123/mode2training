package com.eLibrary.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "my_books")
public class Books {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(length = 10)
	private Integer bookId;
	@Column(length = 20)
	private String bookName;
	@Column(length = 20)
	private String author;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "userId") // Foreign key
	@JsonBackReference
	private User user;

	public Integer getBookId() {
		return bookId;
	}

	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Books(Integer bookId, String bookName, String author, User user) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.author = author;
		this.user = user;
	}

	public Books() {
		super();
	}
}
