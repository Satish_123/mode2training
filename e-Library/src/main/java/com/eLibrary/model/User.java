package com.eLibrary.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sun.istack.NotNull;

@Entity
@Table(name = "my_user")
@JsonIgnoreProperties(ignoreUnknown=true)
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(length=10)
	private Integer userId;
	@NotNull
	@Size(min=3,message="Name should be min 3 characters")
	@Column(length = 20)
	private String userName;
	@Column(length = 10)
	private Integer contactNumber;
	@Column(length = 30)
	@NotNull
	@Size(min=3,message="Name should be min 3 characters")
	private String email;
	@Column(length = 20)
	private String place;
	@Column(length = 20)
	private String country;

	@JsonManagedReference
	@OneToMany(mappedBy = "user", cascade = { CascadeType.ALL })
	private List<Books> books;

	public User() {
		super();
	}

	public User(Integer userId, String userName, Integer contactNumber, String email, String place, String country,
			List<Books> books) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.contactNumber = contactNumber;
		this.email = email;
		this.place = place;
		this.country = country;
		this.books = books;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(Integer contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public List<Books> getBooks() {
		return books;
	}

	public void setBooks(List<Books> books) {
		this.books = books;
	}

}
